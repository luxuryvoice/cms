<?php
class LV {
    public function post ($url, $data) {
        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Content-Length: '.strlen($data)
            ],
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data
        ]);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}
$lv = new LV();
$result = $lv->post(
//    "http://192.168.1.105:8119/lv/api/lv/indexTest/test",
    "http://www.qimiaodian.com:8119/lv/api/lv/indexFashion/addSpecialCards",
    json_encode([[
        description => "hhh",
        sid => 1,
        status => 0,
        rid => 1,
        name => "名称1",
        cardType => 1,
        coverUrl => "http://www.baidu.com"
    ]])
);
print_r(json_encode($result));

?>