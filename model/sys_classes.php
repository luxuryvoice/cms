<?php
class LV {
    public $server_path_local = "/project/luxuryvoicecms/";
    public $server_path_image = "http://www.qimiaodian.com:8120/";
    public $upload_image = "http://www.qimiaodian.com:8120/WiseUpload/media/upload/imageCategory/activity/0";
    public $api = [
        activity => [
            addMain => "http://www.qimiaodian.com:8119/lv/api/lv/activity/addActivity",
            addDetail => "http://www.qimiaodian.com:8119/lv/api/lv/activity/addActivityDetail",
            addRelativeActivity => "http://www.qimiaodian.com:8119/lv/api/lv/activity/addRelativeActivity",
            listAll => "http://www.qimiaodian.com:8119/lv/api/lv/activity/activities/",
            statusChange => "http://www.qimiaodian.com:8119/lv/api/lv/activity/updateActivityStatus/",
			getMain => "http://www.qimiaodian.com:8119/lv/api/lv/activity/details/",
			getDetail => "http://www.qimiaodian.com:8119/lv/api/lv/activity/contents/",
            getRelativeActivity => "http://www.qimiaodian.com:8119/lv/api/lv/activity/queryRelativeActivity/",
            getRelativeProduct => "http://www.qimiaodian.com:8119/lv/api/lv/activity/queryRelativeProduct/",
            searchByname => "http://www.qimiaodian.com:8119/lv/api/lv/activity/queryActivityByName/"
        ],
        product => [
            addMain => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/addJewelry",
            addDetail => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/addProductImage/",
            addRelativeProduct => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/addProductToProduct",
			getProduct => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryProduct/",
            listAll => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryJewelry/",
            listDesigner => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/designer/",
            listBrand => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/brands/",
            listCategory => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryCategory/",
            listEmbed => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryEmbed/",
            listMaterial => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryMaterial/",
            listOrigin => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryOrigin/",
            listStyle => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryStyle/",
            searchByname => "http://www.qimiaodian.com:8119/lv/api/lv/indexProduct/queryProductByName/"
        ],
        special => [
            addMain => "http://www.qimiaodian.com:8119/lv/api/lv/indexFashion/addSpecial",
            addDetail => "http://www.qimiaodian.com:8119/lv/api/lv/indexFashion/addSpecialCards"
        ]
    ];
    
    public function get ($url) {
        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 5
        ]);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
    
    public function post ($url, $data) {
        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data
        ]);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
    
    public function postjson ($url, $data) {
        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_HTTPHEADER => [
            'Content-Type: application/json',
            'Content-Length: '.strlen($data)
            ],
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data
        ]);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}
?>