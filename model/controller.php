<?php
include "sys_classes.php";

$lv = new LV();
switch ($_GET["type"]) {
//activity
    case "activityAddMain": 
        $result = $lv->post(
            $lv->api["activity"]["addMain"],
            [
                subject => $_POST["subject"],
                headpic => $_POST["headpic"],
                name => $_POST["name"],
                slogan => $_POST["slogan"],
                address => $_POST["address"],
                beginDateTime => $_POST["beginDateTime"],
                endDateTime => $_POST["endDateTime"],
                eid => "1"
            ]
        );
        break;
    case "activityAddDetail": 
        $result = $lv->post(
            $lv->api["activity"]["addDetail"],
            [
                content => $_POST["content"],
                type => "0",
                aid => $_POST["aid"]
            ]
        );
        break;
    case "activityAddRelativeActivity": 
        $result = $lv->post(
            $lv->api["activity"]["addRelativeActivity"],
            [
                maid => $_POST["aid"],
                raid => $_POST["pid"]
            ]
        );
        break;
    case "activityListAll": 
        $result = $lv->get(
            $lv->api["activity"]["listAll"].$_GET["status"]."/".$_GET["page"]
        );
        break;
    case "activityStatusChange": 
        $result = $lv->post(
            $lv->api["activity"]["statusChange"].$_POST["aid"],
            [
                status => $_POST["status"],
                content => $_POST["content"]
            ]
        );
        break;
	case "activityGetMain": 
        $result = $lv->get(
            $lv->api["activity"]["getMain"].$_GET["aid"]
        );
        break;
	case "activityGetDetail": 
        $result = $lv->get(
            $lv->api["activity"]["getDetail"].$_GET["aid"]
        );
        break;
    case "activityGetRelativeActivity": 
        $result = $lv->get(
            $lv->api["activity"]["getRelativeActivity"].$_GET["aid"]
        );
        break;
    case "activityGetRelativeProduct": 
        $result = $lv->get(
            $lv->api["activity"]["getRelativeProduct"].$_GET["aid"]
        );
        break;
    case "activitySearchByname": 
        $result = $lv->post(
            $lv->api["activity"]["searchByname"],
            [
                page => $_POST["page"],
                name => $_POST["name"]
            ]
        );
        break;
//product
    case "productAddMain": 
        $result = $lv->post(
            $lv->api["product"]["addMain"],
            [
                name => $_POST["name"],
                price => $_POST["price"],
                description => $_POST["description"],
                category => $_POST["category"],
                designer => $_POST["designer"],
                brand => $_POST["brand"],
                origin => $_POST["origin"],
                style => $_POST["style"],
                material => $_POST["material"],
                embed => $_POST["embed"],
                length => $_POST["length"],
                size => $_POST["size"],
                isRent => $_POST["isRent"],
                postscript => $_POST["postscript"],
                coverUrl => $_POST["coverUrl"],
                type => "1",
                status => "1"
            ]
        );
        break;
    case "productAddDetail": 
        $result = $lv->post(
            $lv->api["product"]["addDetail"].$_GET["rid"],
            [
                path => $_POST["path"],
                type => $_POST["type"],
                description => $_POST["description"]
            ]
        );
        break;
    case "productAddRelativeProduct": 
        $result = $lv->post(
            $lv->api["product"]["addRelativeProduct"],
            [
                mid => $_POST["aid"],
                rid => $_POST["pid"],
                status => 0
            ]
        );
        break;
    case "productListAll": 
        $result = $lv->get(
            $lv->api["product"]["listAll"].$_GET["status"]."/".$_GET["page"]
        );
        break;
	case "productGetSingle": 
        $result = $lv->get(
            $lv->api["product"]["getProduct"].$_GET["pid"]
        );
        break;
    case "productListDesigner":
        $result = $lv->get(
            $lv->api["product"]["listDesigner"].$_GET["ptype"]."/".$_GET["page"]
        );
        break;
    case "productListBrand":
        $result = $lv->get(
            $lv->api["product"]["listBrand"].$_GET["ptype"]."/".$_GET["page"]
        );
        break;
    case "productListCategory":
        $result = $lv->get(
            $lv->api["product"]["listCategory"].$_GET["page"]
        );
        break;
    case "productListEmbed":
        $result = $lv->get(
            $lv->api["product"]["listEmbed"].$_GET["page"]
        );
        break;
    case "productListMaterial":
        $result = $lv->get(
            $lv->api["product"]["listMaterial"].$_GET["page"]
        );
        break;
    case "productListOrigin":
        $result = $lv->get(
            $lv->api["product"]["listOrigin"].$_GET["page"]
        );
        break;
    case "productListStyle":
        $result = $lv->get(
            $lv->api["product"]["listStyle"].$_GET["page"]
        );
        break;
    case "productSearchByname":
        $result = $lv->post(
            $lv->api["product"]["searchByname"],
            [
                page => $_POST["page"],
                name => $_POST["name"]
            ]
        );
        break;
//special
    case "specialAddMain":
        $result = $lv->post(
            $lv->api["special"]["addMain"],
            [
                subject => $_POST["subject"],
                description => $_POST["description"],
                coverUrl => $_POST["coverUrl"],
                lid => 1
            ]
        );
        break;
    case "specialAddDetail":
        $result = $lv->postjson(
            $lv->api["special"]["addDetail"],
            json_encode([[
                description => $_POST["description"],
                sid => $_POST["sid"],
                status => $_POST["status"],
                rid => $_POST["rid"],
                name => $_POST["name"],
                cardType => $_POST["cardType"],
                coverUrl => $_POST["coverUrl"]
            ]])
        );
        break;
}
print_r(json_encode($result));

?>