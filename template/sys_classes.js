"use strict";
var LV = {};

LV.serverPath = {
    local: "/project/luxuryvoicecms/",
    image: "http://img.qimiaodian.com:8088"
};

LV.api = LV.serverPath.local + "model/controller.php";

LV.uploadImage = LV.serverPath.local + "upload/WiseUpload/media/upload/imageCategory/";

LV.getDom = function (id) {
    return document.getElementById(id);
};

LV.each = function (array, callback) {
    var i;
    for (i = 0; i < array.length; i += 1) {
        callback(i, array[i]);
    }
};

LV.click = function (dom) {
    var event = document.createEvent("HTMLEvents");
    event.initEvent("click", true, false);
    dom.dispatchEvent(event);
};

LV.xhrget = function (url, callback, errorcallback) {
    var xhr = new XMLHttpRequest(),
        data;
    xhr.open("GET", url, true);
    xhr.send();
    xhr.onreadystatechange = function (event) {
        if (xhr.readyState === 4) {
            data = JSON.parse(xhr.responseText);
            switch (data.status) {
            case "0":
                callback(data.body);
                break;
            case "1":
                if (errorcallback !== undefined) {
                    errorcallback(data.msg);
                }
                break;
            }
        }
    };
};

LV.xhrpost = function (url, param, callback, errorcallback) {
    var xhr = new XMLHttpRequest(),
        data;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(param);
    xhr.onreadystatechange = function (event) {
        if (xhr.readyState === 4) {
            data = JSON.parse(xhr.responseText);
            switch (data.status) {
            case "0":
                callback(data.body);
                break;
            case "1":
                if (errorcallback !== undefined) {
                    errorcallback(data.msg);
                }
                break;
            }
        }
    };
};

LV.xhrfile = function (url, param, callback, errorcallback) {
    var xhr = new XMLHttpRequest(),
        data;
    xhr.open("POST", url, true);
    xhr.send(param);
    LV.getDom("loader").style.display = "block";
    xhr.onreadystatechange = function (event) {
        if (xhr.readyState === 4) {
            data = JSON.parse(xhr.responseText);
            switch (data.status) {
            case "0":
                callback(data.body);
                break;
            case "1":
                if (errorcallback !== undefined) {
                    errorcallback(data.msg);
                }
                break;
            }
            LV.getDom("loader").style.display = "none";
        }
    };
};