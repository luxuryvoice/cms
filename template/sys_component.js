class Breadcrumb {
    constructor(object) {
        this.originDom = object.dom;
        this.sectionArray = object.section;
        this.sectionDomArray = [];
        this.render();
    }
    render() {
        var dom = "";
        this.sectionArray.forEach(element => dom += `<li class="breadcrumb-item">${element}</li>`);
        this.originDom.innerHTML = `<ol class="breadcrumb">${dom}</ol>`;
        this.sectionDomArray = Array.from(this.originDom.querySelectorAll("li"));
    }
    select(x) {
        this.sectionDomArray.forEach(function (element, index) {
            element.className = (index <= x) ? "breadcrumb-item active" : "breadcrumb-item";
        });
    }
}

class Form {
    constructor(object) {
        this.originDom = object.dom;
        this.sectionArray = object.section;
        this.inputDomArray = [];
        this.fileFormDom;
        this.fileInputDom;
        this.fileReturnDom;
        this.filePreviewDom;
        this.render();
    }
    render() {
        var dom = "",
            fileForm = "",
            formid,
            inputid,
            urlid,
            previewid;
        this.sectionArray.forEach(function (element, index) {
            switch (element.type) {
            case "text":
            case "number":
            case "date":
                dom += `<div class="form-group">
                            <label for="${element.id}" class="col-sm-2 control-label">${element.text}</label>
                            <div class="col-sm-5">
                                <input type="${element.type}" class="form-control" id="${element.id}" placeholder="请输入${element.text}" name="${element.name}">
                            </div>
                        </div>`;
                break;
            case "select":
                dom += `<div class="form-group">
                            <label for="${element.id}" class="col-sm-2 control-label">${element.text}</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="${element.id}" name="${element.name}"></select>
                            </div>
                        </div>`;
                break;
            case "radio":
                var sub = "";
                element.section.forEach(function (ele, idx) {
                    sub += `<label class="radio-inline">
                                <input type="radio" name="${element.name}" id="${element.id}${idx}" value="${ele.value}" ${ele.checked}>${ele.text}
                            </label>`;
                });
                dom += `<div class="form-group">
                            <label for="${element.id}0" class="col-sm-2 control-label">${element.text}</label>
                            <div class="col-sm-5">${sub}</div>
                        </div>`;
                break;
            case "image":
                dom += `<div class="form-group">
                            <label for="${element.id}" class="col-sm-2 control-label">${element.text}</label>
                            <div class="col-sm-3">
                                <label for="${element.id}" class="thumbnail">
                                    <img id="${element.previewid}" src="../images/uploadImage.png" alt="点击上传图片">
                                </label>
                                <input type="text" class="form-control sr-only" id="${element.id}URL" name="${element.name}">
                            </div>
                        </div>`;
                fileForm += `<form class="sr-only" id="formFile">
                                <input type="file" class="form-control sr-only" id="${element.id}" name="src">
                            </form>`;
                inputid = element.id;
                urlid = element.id + "URL";
                previewid = element.previewid;
            }
        });
        this.originDom.innerHTML = `<form class="form-horizontal">${dom}</form>${fileForm}`;
        this.inputDomArray = Array.from(this.originDom.querySelectorAll("input")).concat(Array.from(this.originDom.querySelectorAll("select")));
        this.fileFormDom = this.originDom.querySelectorAll(`#formFile`)[0];
        this.fileInputDom = this.originDom.querySelectorAll(`#${inputid}`)[0];
        this.fileReturnDom = this.originDom.querySelectorAll(`#${urlid}`)[0];
        this.filePreviewDom = this.originDom.querySelectorAll(`#${previewid}`)[0];
    }
    getSerialize() {
        var string = "";
        this.inputDomArray.forEach(function (element, index) {
            if (!(element.type == "radio" && element.hasAttribute("checked"))) {
                string += `${element.name}=${encodeURIComponent(element.value)}&`;
            }
        });
        return string;
    }
    fillSelection(id, object) {
        var dom = this.originDom.querySelectorAll(`#${id}`)[0];
        dom.innerHTML += `<option value="${object.id}">${object.name}</option>`;
    }
}

class DoublePanel {
    constructor(object) {
        this.originDom = object.dom;
        this.titleLeft = object.titleLeft;
        this.titleRight = object.titleRight;
        this.listGroupLeft;
        this.listGroupRight;
        this.btnAdd;
        this.btnRemove;
        this.itemArray;
        this.selectArray;
        this.render();
    }
    render() {
        var dom = "";
        dom += `<div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">${this.titleLeft}</div>
                        <ul class="list-group"></ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">${this.titleRight}</div>
                        <ul class="list-group"></ul>
                    </div>
                </div>`;
        this.originDom.innerHTML = dom;
        this.listGroupLeft = this.originDom.querySelectorAll(`.list-group`)[0];
        this.listGroupRight = this.originDom.querySelectorAll(`.list-group`)[1];
    }
    fillListGroupLeft(data) {
        var dom = "",
            list = [];
        data.forEach(function (element, index) {
            dom += `<li class="list-group-item" pid="${element.id}">${((element.subject !== undefined) ? element.subject : element.name)}<span class="glyphicon glyphicon-ok pull-right btn-add" aria-hidden="true"></span></li>`;
        });
        this.listGroupLeft.innerHTML = dom;
        this.itemArray = data;
        this.bindOperateAdd();
    }
    bindOperateAdd() {
        var dom = this.listGroupRight,
            btnAdd = Array.from(this.listGroupLeft.querySelectorAll(`.btn-add`)),
            data = this.itemArray,
            root = this;
        btnAdd.forEach(function (element, index) {
            element.onclick = function () {
                dom.innerHTML += `<li class="list-group-item" data='${JSON.stringify(data[index])}' pid="${data[index].id}">${((data[index].subject !== undefined) ? data[index].subject : data[index].name)}<span class="glyphicon glyphicon-remove pull-right btn-remove" aria-hidden="true"></span></li>`;
                root.bindOperateRemove();
            };
        });
    }
    bindOperateRemove() {
        var dom = this.listGroupRight,
            btnRemove = Array.from(this.listGroupRight.querySelectorAll(`.btn-remove`));
        btnRemove.forEach(function (element, index) {
            element.onclick = function () {
                btnRemove = Array.from(dom.querySelectorAll(`.btn-remove`));
                dom.removeChild(dom.childNodes[btnRemove.indexOf(element)]);
            };
        });
    }
    getSelectedItem() {
        var array = [];
        Array.from(this.listGroupRight.querySelectorAll("li")).forEach(function (element, index) {
            array.push({
                pid: element.getAttribute("pid"),
                subject: element.innerText,
				data: JSON.parse(element.getAttribute("data"))
            })
        });
        return this.selectArray = array;
    }
}

class Dropdown {
    constructor(object) {
        this.originDom = object.dom;
        this.title = object.title;
        this.sectionArray = object.section;
        this.buttonDomArray;
        this.render();
    }
    render() {
        var dom = "";
        this.sectionArray.forEach(function (element, index) {
            switch (element.type) {
            case "item":
                dom += `<li><a class="imagegroup-btn" href="javascript:void(0)">${element.text}</a></li>`;
                break;
            case "divider":
                dom += `<li role="separator" class="divider"></li>`;
                break;
            }
        });
        this.originDom.innerHTML =
            `<div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${this.title}<span class="caret"></span></button>
                <ul class="dropdown-menu">${dom}</ul>
            </div>`;
        this.buttonDomArray = Array.from(this.originDom.querySelectorAll(".imagegroup-btn"));
    }
}
class PanelGroup {
    constructor(object) {
        this.originDom = object.dom;
        this.sectionArray = object.section;
        this.panelBodyDomArray;
        this.render();
    }
    render() {
        var dom = "";
        this.sectionArray.forEach(function (element, index) {
            dom +=
                `<div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="panel-title">${element}</p>
                    </div>
                    <div class="panel-body"></div>
                </div>`;
        });
        this.originDom.innerHTML = dom;
        this.panelBodyDomArray = Array.from(this.originDom.querySelectorAll(".panel-body"));
    }
}

class ListSearch {
    constructor(object) {
        this.originDom = object.dom;
        this.type = object.type;
        this.render();
    }
    render() {
        var dom = "";
        this.originDom.innerHTML = `<div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="查找${this.type}">
                                    <span class="input-group-btn">
                                        <button id="" class="btn btn-default" type="button">
                                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>`;
    }
}

class List {
    constructor(object) {
        this.originNavDom = object.navDom,
        this.originTableDom = object.tableDom,
        this.navTabs = object.tabs,
        this.trName = object.columns,
        this.sectionDomArray = [],
        this.render();
    }
    render() {
        var navDom = "",
            tableDom = "";
        this.navTabs.forEach(element => navDom += `<li role="presentation"><a href="javascript:void(0)">${element}</a></li>`);
        this.originNavDom.innerHTML = `<div class="form-group clearfix">
                                    <div class="col-sm-12">
                                        <ul class="nav nav-tabs">${navDom}</ul>
                                    </div>
                                </div>`;
        this.sectionDomArray = Array.from(this.originNavDom.querySelectorAll("li"));
        this.trName.forEach(element => tableDom += `<td>${element}</td>`);
        this.originTableDom.innerHTML = `<div class="form-group clearfix">
                                    <div class="col-sm-12">
                                        <div class="table-box">
                                            <table class="table table-hover">
                                                <thead><tr>${tableDom}</tr></thead>
                                            	<tbody></tbody></table>
                                            <div class="text-center">
                                                <a href="javascipt:void(0)" class="hasmore">查看更多</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;
    }
	select(x) {
		this.sectionDomArray.forEach(function (element, index) {
            element.className = (index === x) ? "active" : "";
		});
		this.originTableDom.getElementsByTagName("tbody")[0].innerHTML = "";
		return 0;
	}
}

class Dialog {
    constructor(object) {
        this.originDom = object.dom;
        this.title = object.title;
        this.hasSubmitBtn = object.hasSubmitBtn;
        this.btnClose;
        this.panelBodyDom;
        this.render();
    }
    render() {
        var dom = "";
        dom = `<div class="dialog">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">${this.title}
                            <span class="glyphicon glyphicon-remove pull-right btn-close" aria-hidden="true"></span>
                            </h3>
                        </div>
                        <div class="panel-body"></div>
                    </div>
                </div>`;
        this.originDom.innerHTML = dom;
        this.panelBodyDom = this.originDom.querySelectorAll(".panel-body")[0];
        this.btnClose = this.originDom.querySelectorAll(".btn-close")[0];
        this.bindOperateClose();
    }
    bindOperateClose() {
        var originDom = this.originDom;
        this.btnClose.onclick = function () {
            originDom.style.display = "none";
        };
    }
}